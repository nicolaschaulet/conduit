![ConduitLogo.png](https://bitbucket.org/repo/xkAgBq/images/990560140-ConduitLogo.png)
### What is Conduit? ###

Conduit is an open source plug-in authored by [Proving Ground.](http://ProvingGround.io) Conduit enables designers to create custom data visualizations and heads up displays that update with your parametric models. 

### Who maintains Conduit? ###

* Conduit is authored and maintained by [Proving Ground's own Dave Stasiuk](http://provingground.io/about/dave-stasiuk/)
* If you have questions, visit our [Conduit Website](http://provingground.io/tools/conduit-for-grasshopper/)

### How do I get started? ###

* Current "official" Conduit builds can be installed through a [Click Once installer link.](http://provingground-conduit.s3-website-us-west-2.amazonaws.com/ProvingGround.Conduit.installer.application)
* You can also build the Conduit project from the source code (Visual Studio 2013 C# Project)

Requirements:

* [Rhinoceros 5.0 (64-Bit)](https://www.rhino3d.com/)
* [Grasshopper](http://www.grasshopper3d.com/)

### Where can I learn how to use Conduit? ###
[Take one of our Data-Driven Design workshops!](https://provingground.io/services/education/data-driven-design-workshops/)

### How can I contribute? ###

If you have come across bugs or have wish list items, [submit them to the Issues section of the Conduit repo.](https://bitbucket.org/archinate/conduit/issues?status=new&status=open)

If you have built some cool stuff for Conduit and would like to share it back with the official project, you can follow these steps...

*  Fork the Conduit repository
*  Make cool stuff.
*  Submit a Pull request.

### Thanks! ###
We'd like to say thanks to....

* [HDR's](http://www.hdrinc.com) Practice Innovation team.  In particular, we'd like to thank Matt Goldsberry and Joel Yow for testing the tool.
* [Robert McNeel & Associates](http://www.en.na.mcneel.com/) for providing great tools that can be customized and extended.
* Andrew Heumann for some inspiration in creating Rhino viewport graphics.  Check out [his amazing Human plugin.](http://www.grasshopper3d.com/group/human) 

### The MIT License (MIT) ###

Conduit is an open source project under the [MIT](http://opensource.org/licenses/MIT) license

Copyright (c) 2016 Proving Ground LLC

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.