﻿//using GH_IO;
//using GH_IO.Serialization;
//using Grasshopper;
//using Grasshopper.Kernel;
//using Grasshopper.Kernel.Data;
//using Grasshopper.Kernel.Types;


//using Rhino;
//using Rhino.Collections;
//using Rhino.Geometry;

//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Drawing;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Windows.Forms;

//using ProvingGround.Conduit.Classes;
//using ProvingGround.Conduit.Utils;

//namespace ProvingGround.Conduit
//{
//    public class nodeChart : GH_Component
//    {
//        #region Register Node

//        /// <summary>
//        /// Load Node Template
//        /// </summary>
//        public nodeChart()
//            : base("Draw Chart", "HUD Chart", "Draw a chart in the HUD", "Proving Ground", "HUD")
//        {

//        }

//        /// <summary>
//        /// Component Exposure
//        /// </summary>
//        public override GH_Exposure Exposure
//        {
//            get { return GH_Exposure.primary; }
//        }

//        /// <summary>
//        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
//        /// </summary>
//        public override Guid ComponentGuid
//        {
//            get { return new Guid("a4882e8d-e01d-4929-ac54-445c558121ed"); }
//        }

//        /// <summary>
//        /// Icon 24x24
//        /// </summary>
//        protected override Bitmap Icon
//        {
//            get { return Properties.Resources.PGIconSample; }
//        }
//        #endregion

//        #region Inputs/Outputs
//        /// <summary>
//        /// Node inputs
//        /// </summary>
//        /// <param name="pManager"></param>
//        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
//        {
//            pManager.AddRectangleParameter("Chart Boundary", "Bounds", "The boundary for the chart", GH_ParamAccess.item);
//            pManager.AddNumberParameter("Data", "Data", "DataTree of chart values (one path for each series)", GH_ParamAccess.tree);
//        }

//        /// <summary>
//        /// Node outputs
//        /// </summary>
//        /// <param name="pManager"></param>
//        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
//        {
//            pManager.Register_GenericParam("Chart object", "DrawObj", "The component output");
//        }
//        #endregion

//        //#region Menus
//        ///// <summary>
//        ///// Sample Menu item
//        ///// </summary>
//        ///// <param name="menu"></param>
//        ///// <returns></returns>
//        //public override bool AppendMenuItems(ToolStripDropDown menu)
//        //{
//        //    Menu_AppendItem(menu, "Sample Menu Item", Menu_MyCustomItemClicked);
//        //    return true;
//        //}

//        ///// <summary>
//        ///// Menu item clicked
//        ///// </summary>
//        ///// <param name="sender"></param>
//        ///// <param name="e"></param>
//        //private void Menu_MyCustomItemClicked(object sender, EventArgs e)
//        //{
//        //    //Custom Menu Code
//        //}
//        //#endregion

//        #region Solution
//        /// <summary>
//        /// Code by the component
//        /// </summary>
//        /// <param name="DA"></param>
//        protected override void SolveInstance(IGH_DataAccess DA)
//        {
//            // Solution

//            //Variables
//            Rectangle3d B = new Rectangle3d();
//            DA.GetData(0, ref B);

//            GH_Structure<GH_Number> D = new GH_Structure<GH_Number>();
//            DA.GetDataTree<GH_Number>(1, out D);

//            if (!D.Any())
//            {
//                D = new GH_Structure<GH_Number>();
//                D.Append(new GH_Number(0), new GH_Path(0));
//                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "No data has been added for evaluation");
//            }

//            List<List<double>> m_values = new List<List<double>>();

//            foreach (GH_Path m_thisPath in D.Paths)
//            {
//                var m_thisBranch = new List<double>();
//                D.Branches[D.Paths.IndexOf(m_thisPath)].ForEach(v => m_thisBranch.Add(v.Value));
//                m_values.Add(m_thisBranch);
//            }

//            //Solution

//            clsChartStyle m_chartStyle =  new clsChartStyle() {styleName="Default Chart", unset = true};
//            clsPaletteStyle m_paletteStyle = new clsPaletteStyle() {styleName="Default Palette", unset = true};

//            DrawChart m_thisChart = new DrawChart(
//                B, // boundary
//                m_values, // salues
//                new acStyle[] { (acStyle)m_chartStyle, (acStyle)m_paletteStyle } // styles
//                );

//            //Output
//            DA.SetData(0, m_thisChart as iDrawObject);

//        }
//        #endregion
//    }
//}

